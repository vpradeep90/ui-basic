// this method is used to create the user based on the role 
function createUser() {
    console.log("enter into create user");
    //vallidating user name here
    const name = document.getElementById("uname").value;
    if (name === "" && name.length === 0) {
        alert("Name must be filled");
        return false;
    }
   

    //code that allows only dbs  or hcl emails 
    const email = document.getElementById("email").value;
    var mailformat = "[a-z0-9._%+-]+@[hcl|dbs]+\.[a-z]{2,}$";
    if (email.match(mailformat)) {
    }
    else {
        alert("Enter vallid email id...(Use only DBS or HCL Domain)");
        return false;
    }
     /* phone number validation */
     const phoneNumber = document.getElementById("phone").value;
     if (phoneNumber.length === 10) {
     }
     else {
         alert("Phone number must be filled with 10 digits")
         return false;
     }
     var role = document.getElementById("role").value;
     if (role === "" && role.length === 0) {
         alert("role must be selected");
         return false;
     }
       /* creating user object */
       console.log("vallidation done");
    
  
       // password:retVal

       // logic to generate password 
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
         pwd = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        pwd += charset.charAt(Math.floor(Math.random() * n));
    }
    console.log("pass word created")
    let user = {name: name,phone: phoneNumber,email: email,role: role,password: pwd};
    console.log(user);
    var httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest();
    }
    else {
        httpRequest = new ActiveXObject();
    }
    httpRequest.onreadystatechange =  function() {
        if (this.status === 201 && this.readyState === 4) {
            window.alert("user added, please login!");
            window.location.assign('login.html');
        }
    }
        /* api call to  send the user data to server */
    httpRequest.open('post', 'http://localhost:3000/users', false);
    httpRequest.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    httpRequest.send(JSON.stringify(user));
}
//This function checks for the user existance and if the user doesn't exist the user gets registered
function checkAndRegisterUser()
{
    debugger
    var email = document.getElementById("email").value;

    var httpReq;
    if(window.XMLHttpRequest)
    {
    httpReq = new XMLHttpRequest
    }
    else{
    httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }


    httpReq.onreadystatechange = function()
    {
        if(this.readyState===4 && this.status===200)
        {
            var data = JSON.parse(this.response);
            var len = data.length;
            if(len>0)
            {
                alert("User with given email already exists.");
                return false;
            }
            else
            createUser();
        }
    } 
    
    
    httpReq.open('GET',"http://localhost:3000/users?email="+email,false);
    httpReq.send();

}
