//This method adds a slot for the doctor
function addSlot()
{
    var date = document.getElementById("date").value;
    var time = document.getElementById("time").value;
    var specialist = document.getElementById("specialist").value;

    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpReq.onreadystatechange = function()
    {
            if(this.readyState===4 && this.status===201)
            {
                    alert("Slot added successfully");
                    window.location.assign("success.html");
                
            }
    } 

    var obj = {doctoremail:sessionStorage.getItem('user'),date:date,time:time,specialist:specialist,booked:"no"};

    console.log(obj);
 
     httpReq.open('POST',"http://localhost:3000/slots",true);
     httpReq.setRequestHeader("Content-type","application/json");
     httpReq.send(JSON.stringify(obj));
 


}
//This method checks for the existance of the slot for the doctor
function checkSlotExistanceAndAddSlot()
{
    var date = document.getElementById("date").value;
    var time = document.getElementById("time").value;
    var specialist = document.getElementById("specialist").value;
    var email = sessionStorage.getItem('user');

    //Validation starts here
    var curDate = new Date();
    if(date===null||!date.length>0){
         alert("Please Provide date for booking slot ");
    return false;
    }
    else if (new Date(date).getTime() <= curDate.getTime()) {
         alert("The Date must be equal or later than the current date");
    return false;
    }
    
    if(time===null||!time.length>0){
    alert("Please Provide time for booking slot");
    return false;
    }
    //Validation ends here
		


    var httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

    httpReq.onreadystatechange = function()
    {
            if(this.readyState===4 && this.status===200)
            {
                var data = JSON.parse(this.response);
                var len = data.length;
                if(len>0)
                {
                    alert("Slot already available for the given Date,Time and Specialization.");
                    return false;
                }
                else
                   addSlot();
            }
    } 

    
    httpReq.open('GET',"http://localhost:3000/slots?date="+date+"&time="+time+"&specialist="+specialist+"&doctoremail="+email,true);
    httpReq.send();



}
//This function logs out of the application
function logout()
{
    window.location.assign("index.html");
}